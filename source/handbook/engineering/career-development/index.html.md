---
layout: markdown_page
title: "Engineering Career Development"
---

## Engineering Roles at GitLab

Most important is the fork between purely technical work and managing teams. It's important that Engineering Managers self-select into that track and don't feel pressured. We believe that management is a craft like any other and requires dedication. We also believe that everyone deserves a manager that is passionate about their craft.

Once someone reaches Senior Software Engineer and wants to progress, they will need to decide whether they want to remain purely technical or pursue managing technical teams. Their manager can provide opportunities to try tasks from both tracks if they wish. Staff Software Engineers and Engineering Managers are equivalent in terms of base compensation and prestige.

<table style="text-align:center;">
  <tr>
    <td colspan="4" style="border:0;">&nbsp;</td>
    <td rowspan="2" style="border:0; vertical-align:middle;"><a href="/jobs/developer#staff-developer">Staff Developer</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle;"><a href="/jobs/developer#distinguished-developer">Distinguished Developer</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle;"><a href="/jobs/developer#engineering-fellow">Engineering Fellow</a></td>
  </tr>
  <tr>
    <td rowspan="2" style="border:0; vertical-align:middle;"><a href="/jobs/developer#developer">Developer</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle;"><a href="/jobs/developer#senior-developer">Senior Developer</a></td>
    <td style="border:0; vertical-align:middle;">↗</td>
  </tr>
  <tr>
    <td style="border:0; vertical-align:middle;">↘</td>
    <td rowspan="2" style="border:0; vertical-align:middle;"><a href="/jobs/engineering-management#engineering-manager">Engineering Manager</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle;"><a href="/jobs/engineering-management#director-of-engineering">Director of Engineering</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle;"><a href="/jobs/engineering-management#vp-of-engineering">VP of Engineering</a></td>
  </tr>
  <tr>
    <td colspan="4" style="border:0;">&nbsp;</td>
  </tr>
</table>

Note that we have a specific section for Senior Software Engineer because it's an important step in the technical development for every engineer. But "Senior" can optionally be applied to any role here indicating superior performance. However, it's not required to pass through "senior" for roles other than Software Engineer. Also note that we occasionally hire Junior Software Engineers if they have demonstrated both aptitude and our values and we believe they will quickly develop into a Software Engineer.
